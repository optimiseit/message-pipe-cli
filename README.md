# MessagePipe CLI

Command line client for the 360dialog messagepipe API.

### Run

```
bin/messagepipe command --parameter
``` 

### Commands

#### Getting API key

```
bin/messagepipe get-api-key --app-id <app-id> --username <username> [--password <password>]
```

If no password is given, it will be asked during execution.

#### Get current webhook

```
bin/messagepipe get-webhook --address <address> --api-key <api-key>
```

#### Set legacy webhook

To migrate an integration back to the old api version.

```
bin/messagepipe set-legacy-webhook --address <address> --api-key <api-key> --app-id <app-id> --msisdn <msisdn>
```

#### List templates

```
bin/messagepipe list-templates --address <address> --api-key <api-key>
```

#### Upload media

```
bin/messagepipe upload-media --address <address> --api-key <api-key> --content-type <content-type> --file <file>
```

#### Download media

```
bin/messagepipe download-media --address <address> --api-key <api-key> --id <id> --file <file>
```

#### Get profile about

```
bin/messagepipe get-profile-about --address <address> --api-key <api-key>
```

#### Set profile about

```
bin/messagepipe set-profile-about --address <address> --api-key <api-key> --about-text <max 139 chars>
```

#### Set profile photo

```
bin/messagepipe set-profile-photo --address <address> --api-key <api-key> --content-type <content-type> --file <file>
```
