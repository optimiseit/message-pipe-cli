<?php

namespace App;

use Exception;
use Optimise\MessagePipe\Hub\ApiKey;
use Optimise\MessagePipe\WhatsApp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ListTemplatesCommand extends Command
{
    protected static $defaultName = 'list-templates';

    protected function configure(): void
    {
        $this
            ->setDescription('List templates')

            ->addOption(
                'address',
                null,
                InputOption::VALUE_REQUIRED,
                'Api address'
            )
            ->addOption(
                'api-key',
                null,
                InputOption::VALUE_REQUIRED,
                'Api key'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @psalm-suppress PossiblyInvalidCast */
        $address = (string)$input->getOption('address');
        /** @psalm-suppress PossiblyInvalidCast */
        $apiKey = (string)$input->getOption('api-key');

        if (!$address || !$apiKey) {
            throw new Exception('Missing parameter(s)');
        }

        $key = new ApiKey($address, $apiKey);

        $whatsAppClient = new Client($key);

        $templates = $whatsAppClient->listTemplates();

        $output->writeln(json_encode($templates, JSON_PRETTY_PRINT));

        return 0;
    }
}
