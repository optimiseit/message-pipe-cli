<?php

namespace App;

use Cache\Adapter\PHPArray\ArrayCachePool;
use Cache\Bridge\SimpleCache\SimpleCacheBridge;
use Exception;
use Optimise\MessagePipe\Hub\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class GetApiKeyCommand extends Command
{
    protected static $defaultName = 'get-api-key';

    protected function configure(): void
    {
        $this
            ->setDescription('Get webhook information')

            ->addOption(
                'username',
                null,
                InputOption::VALUE_REQUIRED,
                'Username'
            )
            ->addOption(
                'password',
                null,
                InputOption::VALUE_REQUIRED | InputOption::VALUE_OPTIONAL,
                'Password'
            )
            ->addOption(
                'app-id',
                null,
                InputOption::VALUE_REQUIRED,
                'AppId'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @psalm-suppress PossiblyInvalidCast */
        $username = (string)$input->getOption('username');
        /** @psalm-suppress PossiblyInvalidCast */
        $password = (string)$input->getOption('password');
        /** @psalm-suppress PossiblyInvalidCast */
        $appId = (string)$input->getOption('app-id');

        if (!$username || !$appId) {
            throw new Exception('Missing parameter(s)');
        }

        $cachePool = new ArrayCachePool();
        $cache = new SimpleCacheBridge($cachePool);

        if (!$password && !$input->isInteractive()) {
            $errOutput = $output instanceof ConsoleOutputInterface ? $output->getErrorOutput() : $output;
            $errOutput->writeln('Missing password');
            return 1;
        }

        while (!$password) {
            $helper = $this->getHelper('question');
            $question = new Question('Password: ');
            $password = (string)$helper->ask($input, $output, $question);
        }

        $hubClient = new Client($cache, $username, $password);

        $apiKey = $hubClient->getOrCreateApiKey($appId);

        $output->writeln(json_encode($apiKey, JSON_PRETTY_PRINT));

        return 0;
    }
}
