<?php

namespace App;

use Exception;
use Optimise\MessagePipe\Hub\ApiKey;
use Optimise\MessagePipe\WhatsApp\Client;
use Optimise\MessagePipe\WhatsApp\Media;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UploadMediaCommand extends Command
{
    protected static $defaultName = 'upload-media';

    protected function configure(): void
    {
        $this
            ->setDescription('Upload media')

            ->addOption(
                'address',
                null,
                InputOption::VALUE_REQUIRED,
                'Api address'
            )
            ->addOption(
                'api-key',
                null,
                InputOption::VALUE_REQUIRED,
                'Api key'
            )
            ->addOption(
                'content-type',
                null,
                InputOption::VALUE_REQUIRED,
                'Content Type'
            )
            ->addOption(
                'file',
                null,
                InputOption::VALUE_REQUIRED,
                'File'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @psalm-suppress PossiblyInvalidCast */
        $address = (string)$input->getOption('address');
        /** @psalm-suppress PossiblyInvalidCast */
        $apiKey = (string)$input->getOption('api-key');
        /** @psalm-suppress PossiblyInvalidCast */
        $contentType = (string)$input->getOption('content-type');
        /** @psalm-suppress PossiblyInvalidCast */
        $fileName = (string)$input->getOption('file');

        if (!$address || !$apiKey || !$contentType || !file_exists($fileName)) {
            throw new Exception('Missing parameter(s)');
        }

        $key = new ApiKey($address, $apiKey);

        $whatsAppClient = new Client($key);

        $stream = fopen($fileName, 'r');

        $id = $whatsAppClient->uploadMedia(
            new Media(
                $contentType,
                $stream
            )
        );

        if (!is_null($id)) {
            $output->writeln($id);
        }

        return 0;
    }
}
