<?php

namespace App;

use Exception;
use Optimise\MessagePipe\Hub\ApiKey;
use Optimise\MessagePipe\WhatsApp\Client;
use Optimise\MessagePipe\WhatsApp\Media;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DownloadMediaCommand extends Command
{
    protected static $defaultName = 'download-media';

    protected function configure(): void
    {
        $this
            ->setDescription('Download media')

            ->addOption(
                'address',
                null,
                InputOption::VALUE_REQUIRED,
                'Api address'
            )
            ->addOption(
                'api-key',
                null,
                InputOption::VALUE_REQUIRED,
                'Api key'
            )
            ->addOption(
                'id',
                null,
                InputOption::VALUE_REQUIRED,
                'Media id'
            )
            ->addOption(
                'file',
                null,
                InputOption::VALUE_REQUIRED,
                'File'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @psalm-suppress PossiblyInvalidCast */
        $address = (string)$input->getOption('address');
        /** @psalm-suppress PossiblyInvalidCast */
        $apiKey = (string)$input->getOption('api-key');
        /** @psalm-suppress PossiblyInvalidCast */
        $id = (string)$input->getOption('id');
        /** @psalm-suppress PossiblyInvalidCast */
        $fileName = (string)$input->getOption('file');

        if (!$address || !$apiKey || !$id) {
            throw new Exception('Missing parameter(s)');
        }

        $key = new ApiKey($address, $apiKey);

        $whatsAppClient = new Client($key);

        $media = $whatsAppClient->downloadMedia($id);
        rewind($media->stream);

        file_put_contents($fileName, $media->stream);

        $output->writeln("Written content of type '{$media->contentType}' to {$fileName}");

        return 0;
    }
}
