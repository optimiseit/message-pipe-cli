<?php

namespace App;

use Exception;
use Optimise\MessagePipe\Hub\ApiKey;
use Optimise\MessagePipe\WhatsApp\Client;
use Optimise\MessagePipe\WhatsApp\Webhook;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SetLegacyWebhookCommand extends Command
{
    protected static $defaultName = 'set-legacy-webhook';

    protected function configure(): void
    {
        $this
            ->setDescription('Set legacy webhook')

            ->addOption(
                'address',
                null,
                InputOption::VALUE_REQUIRED,
                'Api address'
            )
            ->addOption(
                'api-key',
                null,
                InputOption::VALUE_REQUIRED,
                'Api key'
            )
            ->addOption(
                'app-id',
                null,
                InputOption::VALUE_REQUIRED,
                'App Id'
            )
            ->addOption(
                'msisdn',
                null,
                InputOption::VALUE_REQUIRED,
                'Mobile number (4912345678901)'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @psalm-suppress PossiblyInvalidCast */
        $address = (string)$input->getOption('address');
        /** @psalm-suppress PossiblyInvalidCast */
        $apiKey = (string)$input->getOption('api-key');
        /** @psalm-suppress PossiblyInvalidCast */
        $appId = (string)$input->getOption('app-id');
        /** @psalm-suppress PossiblyInvalidCast */
        $msisdn = (string)$input->getOption('msisdn');

        if (!$address || !$apiKey || !$appId || !$msisdn) {
            throw new Exception('Missing parameter(s)');
        }

        $key = new ApiKey($address, $apiKey);

        $whatsAppClient = new Client($key);

        $webhook = $whatsAppClient->setWebhook(new Webhook(
            "https://rt.360dialog.io/xray/events/whatsapp/v1/apps/{$appId}/integrations/{$msisdn}/events"
        ));

        $output->writeln(json_encode($webhook, JSON_PRETTY_PRINT));

        return 0;
    }
}
