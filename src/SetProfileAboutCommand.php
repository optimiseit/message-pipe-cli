<?php

namespace App;

use Exception;
use Optimise\MessagePipe\Hub\ApiKey;
use Optimise\MessagePipe\WhatsApp\Client;
use Optimise\MessagePipe\WhatsApp\Media;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SetProfileAboutCommand extends Command
{
    protected static $defaultName = 'set-profile-about';

    protected function configure(): void
    {
        $this
            ->setDescription('Set profile about')

            ->addOption(
                'address',
                null,
                InputOption::VALUE_REQUIRED,
                'Api address'
            )
            ->addOption(
                'api-key',
                null,
                InputOption::VALUE_REQUIRED,
                'Api key'
            )
            ->addOption(
                'about-text',
                null,
                InputOption::VALUE_REQUIRED,
                'About text'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @psalm-suppress PossiblyInvalidCast */
        $address = (string)$input->getOption('address');
        /** @psalm-suppress PossiblyInvalidCast */
        $apiKey = (string)$input->getOption('api-key');
        /** @psalm-suppress PossiblyInvalidCast */
        $aboutText = (string)$input->getOption('about-text');

        if (!$address || !$apiKey || !$aboutText) {
            throw new Exception('Missing parameter(s)');
        }

        $key = new ApiKey($address, $apiKey);

        $whatsAppClient = new Client($key);

        $whatsAppClient->setProfileAbout($aboutText);

        $about = $whatsAppClient->getProfileAbout();

        $output->writeln(json_encode($about, JSON_PRETTY_PRINT));

        return 0;
    }
}
